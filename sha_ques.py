# /usr/bin/python3
import signal
import sys
import random
import string
import my_secret
from sha256 import SHA256

sys.dont_write_bytecode = True

def alarm(time):
    def handler(signum, frame):
        print("Timeout")
        exit()
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(time)

if __name__ == "__main__":
    alarm(180)
    print("Let's implement SHA-256!!!")
    print("You just need to revise the kernel of algorithm instead of whole one.")
    print("Please refer the sha_imple.py for more information!!")
    print("Follow the instruction of given code to implement SHA-256")
    print("If you have question or suggestion on this homework, plz contact TA CY")
    print("Here is the test!!!!")
    word_set = string.ascii_letters + string.digits
    for i in range(10000):
        s = "".join(random.choice(word_set) for _ in range(random.randint(20, 50)))
        print("Secret is {}".format(s))
        a, my = SHA256(str.encode(s))
        total = input()
        try:
            b, your = total.split("分")
        except ValueError:
            print("Error!!!")
            exit()
        if str(a) != b or str(my) != your:
            print("Error!!!")
            exit()
            break
    print(my_secret.flag)


        
