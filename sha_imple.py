# 

import hashlib
import uuid
from typing import Tuple

import pwn


def right_rotate(x: int, bit_num: int) -> int:
    """
    right rotate implement
    """
    
    return (((x & 0xffffffff) >> (y & 31)) | (x << (32 - (y & 31)))) & 0xffffffff

def right_shift(i: int, bit_num: int) -> int:
    """
    right shift implement
    """
    return i >> bit_num


def SHA256(message: bytes) -> str:
    """
    Do not revise these code below
    """
    h0 = 0x6a09e667
    h1 = 0xbb67ae85
    h2 = 0x3c6ef372
    h3 = 0xa54ff53a
    h4 = 0x510e527f
    h5 = 0x9b05688c
    h6 = 0x1f83d9ab
    h7 = 0x5be0cd19

    k = [0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
         0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
         0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
         0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
         0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
         0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
         0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
         0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2]

    mask = 0xffffffff

    # Preprocessing-padding 
    messageLen = len(message) * 8
    message += b'\x80'
    while(len(message) % 64 != 56):
        message += b'\x00'
    message += messageLen.to_bytes(8, "big")

    intermediate_str = ""
    
    print(message)
    
    
    
    
    
    

    """
    Do not revise these code above
    """

    """
    [MUST READ] Here are some techniques to use
    1. To group a list into n chunks
    2. Convert bytes to int
    3. Do mask operation after any add operations
    Note that the "message" using here is bytes form
    Please reference SHA-2 on wikipedia
    -----------------------------------------------
    Pseudocode and structure of code


    # Process the message in successive 512-bit chunks
    # break message into 512-bit chunks
    for chunk in chunks:
        #
        # Intermediate operation
        #

        for i in range(16, 64):
            #
            # Extend the sixteen 32-bit words into sixty-four 32-bit words
            #

        #
        # Initialize hash value for this chunk
        #

        # Main loop
        for i in range(64):
            #
            # Revise the a, b, c ... , etc.
            #

        # Use code below
        alpha_list = [a, b, c, d, e, f, g, h]
        alpha_str = "|".join("0x{:08x}".format(alpha) for alpha in alpha_list)
        intermediate_str += alpha_str + "$"
        # Use code above

        #
        # Add this chunk's hash to result so far
        #
        
    """
    
        

    """
    Do not revise these code below
    """

    h_list = [h0, h1, h2, h3, h4, h5, h6, h7]
    hash_value = b''.join(h.to_bytes(4, "big") for h in h_list)
    intermediate = hashlib.sha256(str.encode(intermediate_str)).digest()

    return "{0}分{1}".format(hash_value, intermediate)

    """
    Do not revise these code above
    """

"""
Do not revise these code below
[Function] Test the correctness of your code
Please pass the test before sending to server
"""

if __name__ == "__main__":
    for i in range(100000):
        s = uuid.uuid4().hex
        total = SHA256(str.encode(s))
        try:
            a, _ = total.split("分")
        except ValueError:
            exit()
        b = hashlib.sha256(str.encode(s)).digest()
        if a != str(b):
            exit()
            break
    print("Success")

"""
Do not revise code above
"""
